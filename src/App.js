import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Label,modifier} from './component/Label/Label';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Label color={modifier["white"]} >
            React
          </Label>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          <Label color={modifier["red"]} >
            hogehoge
          </Label>
        </header>
      </div>
    );
  }
}

export default App;
