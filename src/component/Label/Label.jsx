import React, { Component } from 'react';
import localstyles from './Label.scss';

export const modifier = {
  "blue": "label--blue",
  "red" : "label",
  "white":"label--white",
}

export const Label =(props)=>{
  return(
    <p className={localstyles[props.color]}>
      {props.children}
    </p>
  )
}