const path = require('path')

module.exports = {
  title: `Atomic Design`,
  showUsage: true,
  getComponentPathLine: (componentPath) => {
    const name = path.basename(componentPath, '.jsx')
    const dir = path.dirname(componentPath)
    return `import ${name} from '${dir}';`
  },
  sections: [
    {
      name: 'Component',
      components: () => ([
        path.resolve(__dirname, 'src/component/Label', 'Label.jsx'),
      ])
    }
  ]
}